from setuptools import setup

setup(
    name='pypicache',
    version='0.1',
    description='PyPI caching and proxying server',
    author='Michael Twomey',
    author_email='mick@twomeylee.name',
    url='http://readthedocs.org/projects/pypicache/',
    packages=['pypicache'],
    package_data={
        'pypicache': [
            'static/*/*',
            'templates/*.html',
        ]
    },
    install_requires=[
        'certifi==1.0.1',
        'chardet==2.2.1',
        'Flask==0.10.1',
        'Jinja2==2.7.2',
        'requests==2.2.1',
        'Werkzeug==0.8.3',
    ]
)
